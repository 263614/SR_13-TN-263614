import csv

with open("oceny_studentow.csv") as file:
    with open("podsumowanie.csv", 'w', newline= '') as file2:
        reader = csv.reader(file)
        writer = csv.writer(file2)
        line = 0
        zaliczenie = 0
        brakZaliczenia = 0
        suma = 0
        liczbaOcen = 0
        ileOsobZOcena = [0, 0, 0, 0, 0, 0, 0]
        for row in reader:
            if line != 0:
                suma += float(row[2]) + float(row[3])
                if float(row[2]) == 2.0:
                    brakZaliczenia += 1
                elif float(row[3]) == 2.0:
                    brakZaliczenia += 1
                else:
                    zaliczenie += 1
                liczbaOcen += 2
                koncowa = (float(row[2]) * 0.4) + (float(row[3]) * 0.6)
                if koncowa < 2.75:
                    ileOsobZOcena[0] += 1
                elif 2.75 <= koncowa < 3.25:
                    ileOsobZOcena[1] += 1
                elif 3.25 <= koncowa < 3.75:
                    ileOsobZOcena[2] += 1
                elif 3.75 <= koncowa < 4.25:
                    ileOsobZOcena[3] += 1
                elif 4.25 <= koncowa < 4.75:
                    ileOsobZOcena[4] += 1
                elif 4.75 <= koncowa < 5.25:
                    ileOsobZOcena[5] += 1
                elif 5.25 <= koncowa < 5.75:
                    ileOsobZOcena[6] += 1
            else:
                line += 1
        print(zaliczenie)
        print(brakZaliczenia)
        print(suma/liczbaOcen)
        print(ileOsobZOcena)
        writer.writerow(["Średnia", suma/liczbaOcen])
        writer.writerow(["LiczbaOsobZ2.0", ileOsobZOcena[0]])
        writer.writerow(["LiczbaOsobZ3.0", ileOsobZOcena[1]])
        writer.writerow(["LiczbaOsobZ3.5", ileOsobZOcena[2]])
        writer.writerow(["LiczbaOsobZ4.0", ileOsobZOcena[3]])
        writer.writerow(["LiczbaOsobZ4.4", ileOsobZOcena[4]])
        writer.writerow(["LiczbaOsobZ5.0", ileOsobZOcena[5]])
        writer.writerow(["LiczbaOsobZ5.5", ileOsobZOcena[6]])
        writer.writerow(["UzyskaloZaliczenie", zaliczenie])
        writer.writerow(["NieUzyskaloZaliczenia", brakZaliczenia])
